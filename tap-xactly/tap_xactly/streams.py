from typing import List
import singer
from singer import metadata

from tap_xactly.client import XactlyClient

LOGGER = singer.get_logger()


class Stream:  # pylint: disable=too-few-public-methods
    tap_stream_id = ""
    key_properties: List[str] = [""]
    replication_method = ""
    valid_replication_keys = [""]
    replication_key = "last_updated_at"
    object_type = ""
    limit = 10000
    selected = True

    def __init__(self, client: XactlyClient, state: dict, stream):
        self.client = client
        self.state = state
        self.schema = stream.schema.to_dict()
        self.metadata = metadata.to_map(stream.metadata)
        self.bookmark_date = "1970-01-11T00:00:01Z"
        self.last_primary = 0

    def sync(self):
        bookmark = singer.get_bookmark(
            self.state,
            self.tap_stream_id,
            self.replication_key,
            self.bookmark_date,
        )

        last_query_record_count = self.limit
        while last_query_record_count >= self.limit:
            try:
                record_count = 0
                records = self.client.query_database(
                    table_name=self.tap_stream_id,
                    limit=self.limit,
                    primary_key=self.key_properties[0],
                    bkmrk_primary=self.last_primary,
                    replication_key=self.replication_key,
                    bkmrk_date=bookmark,
                )
                for record in records:
                    self.bookmark_date = record["MODIFIED_DATE"]
                    self.last_primary = record[self.key_properties[0]]
                    record_count += 1
                    yield record

                last_query_record_count = record_count

            except Exception as ex:  # pylint: disable=broad-except
                LOGGER.warning(f"Client error {ex} :: Closing SQL and Connection.")
                self.client.close_connection()
                LOGGER.info("Restarting Client")
                self.client.setup_connection()
                continue

        LOGGER.info(f"{self.tap_stream_id} sync completed.")
        LOGGER.info(f"Creating bookmark for {self.tap_stream_id} stream")


class IncrementalStream(Stream):  # pylint: disable=too-few-public-methods
    replication_method = "INCREMENTAL"


class FullTableStream(Stream):  # pylint: disable=too-few-public-methods
    replication_method = "FULL_TABLE"


class XcPlanApproval(IncrementalStream):  
    tap_stream_id = "xc_plan_approval"
    key_properties = ["PLAN_APPROVAL_ID"]
    object_type = "XC_PLAN_APPROVAL"
    valid_replication_keys = ["MODIFIED_DATE"]
    replication_key = "MODIFIED_DATE"


class XcRule(IncrementalStream):  
    tap_stream_id = "xc_rule"
    key_properties = ["RULE_ID"]
    object_type = "XC_RULE"
    valid_replication_keys = ["MODIFIED_DATE"]
    replication_key = "MODIFIED_DATE"


class XcPlanRules(IncrementalStream):  
    tap_stream_id = "xc_planrules"
    key_properties = ["PLANRULES_ID"]
    object_type = "XC_PLANRULES"
    valid_replication_keys = ["MODIFIED_DATE"]
    replication_key = "MODIFIED_DATE"

STREAMS = {
    "xc_plan_approval": XcPlanApproval,
    "xc_rule": XcRule,
    "xc_planrules": XcPlanRules,
}